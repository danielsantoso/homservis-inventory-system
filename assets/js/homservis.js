$(document).ready(function() {
    $(".datepicker").pickadate({
        selectMonths: !0,
        selectYears: 70,
        max: !0,
        format: "dd-mm-yyyy"
    });
    var a = $(".datepicker").pickadate(),
        b = a.pickadate("picker");
    $("#calendar-icon").click(function() {
        event.stopPropagation(), b.open(), b.on({
            close: function() {
                $("#item_buy_date").val($(".datepicker").val())
            }
        })
    }), $("ul.tabs").tabs("select_tab", "tab_id"), Dropzone.options.myDropzone = {
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: !0,
        init: function() {
            this.on("addedfile", function(a) {
                fileupload_flag = 1
            }), this.on("complete", function(a) {
                fileupload_flag = 0
            })
        },
        accept: function(a, b) {
            var c = /(?:\.([^.]+))?$/,
                d = c.exec(a.name)[1];
            d = d.toUpperCase(), item_qrcode = $("#item_qrcode").val(), item_qrcode = item_qrcode.toUpperCase(), "" == item_qrcode ? b("Fill the QR Code first!") : "JPG" == d || "JPEG" == d || "PNG" == d || "GIF" == d || "BMP" == d ? b() : b("Please select only supported picture files.")
        },
        sending: function(a, b, c) {
            var d = /(?:\.([^.]+))?$/,
                e = d.exec(a.name)[1];
            e = e.toUpperCase(), item_photo_name = "IMG_" + item_qrcode + "." + e, c.append("fileName", item_photo_name)
        },
        success: function(a, b) {
            var c = JSON.parse(b);
            $("#item_photo").val(c.filename)
        }
    }, Dropzone.options.myDropzone2 = {
        maxFilesize: 10,
        maxFiles: 1,
        addRemoveLinks: !0,
        init: function() {
            this.on("addedfile", function(a) {
                fileupload_flag = 1
            }), this.on("complete", function(a) {
                fileupload_flag = 0
            })
        },
        accept: function(a, b) {
            var c = /(?:\.([^.]+))?$/,
                d = c.exec(a.name)[1];
            d = d.toUpperCase(), item_qrcode = $("#item_qrcode").val(), item_qrcode = item_qrcode.toUpperCase(), "" == item_qrcode ? b("Fill the QR Code first!") : "JPG" == d || "JPEG" == d || "PNG" == d || "GIF" == d || "BMP" == d ? b() : b("Please select only supported picture files.")
        },
        sending: function(a, b, c) {
            var d = /(?:\.([^.]+))?$/,
                e = d.exec(a.name)[1];
            e = e.toUpperCase(), item_photo_name = "TT_" + item_qrcode + "." + e, c.append("fileName", item_photo_name)
        },
        success: function(a, b) {
            var c = JSON.parse(b);
            $("#item_bill").val(c.filename)
        }
    }, $("input#item_qrcode").on({
        keydown: function(a) {
            if (32 === a.which) return !1
        },
        change: function() {
            this.value = this.value.replace(/\s/g, "")
        }
    }), $(".modal-trigger").leanModal(), $("#show_ctg").change(function() {
        $("#filter-form").submit()
    }), $("#show_entry").change(function() {
        $("#filter-form").submit()
    }), $("#sort_by").change(function() {
        $("#filter-form").submit()
      })
});
