<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session', 'form_validation', 'encryption'));
		$this->load->model('login_model');
	}

	public function inventory()
	{
		// check session first
		if (!$this->check_session()) redirect('dashboard/login');

		// load inventory model
		$this->load->model('inventory_model');
		// set default message to null
		$data['msg'] = "";
		// get item id
		$item_id = $this->uri->segment(3);
		// get user role
		$role = $this->session->userdata('user_role');

		// switch role
		switch ($role) {
			case 'ADMIN':
									// get item category
									$data['ctg_data'] = $this->inventory_model->get_ctg();
									// get user data
									$this->load->model('account_model');
									$data['user_data'] = $this->account_model->get_data();

									if (empty($item_id))
									{
										if ($_SERVER['REQUEST_METHOD'] == "POST")
										{
												$data['msg'] = $this->inventory_model->manage();
										}

										// get inventory data
										$show_ctg = "";
										$show_entry = 10;
										$page = 1;

										if (isset($_GET['show_ctg'])) $show_ctg = str_replace("-", " ", $this->security->xss_clean($_GET['show_ctg']));
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_data($show_ctg, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_total_page($show_ctg, $show_entry);

										$this->load->view('admin/add_inventory_view', $data);
									}
									else if ($item_id == "search")
									{
										$keyword = "";
										$show_entry = 10;
										$page = 1;
										if (isset($_GET['keyword'])) $keyword = $this->security->xss_clean($_GET['keyword']);
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_search_data($keyword, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_search_total_page($keyword, $show_entry);
										$this->load->view('admin/add_inventory_view', $data);
									}
									else
									{
										if ($_SERVER['REQUEST_METHOD'] == "POST")
										{
												$data['msg'] = $this->inventory_model->manage();
										}

										$data['item'] = $this->inventory_model->get_data_by_id($item_id);

										if ($data['item'] == FALSE)
											redirect('dashboard/inventory');
										else
											$this->load->view('admin/edit_inventory_view', $data);
									}
				break;
			case 'SUPERVISOR':
									// get item category
									$data['ctg_data'] = $this->inventory_model->get_ctg();
									// get user data
									$this->load->model('account_model');
									$data['user_data'] = $this->account_model->get_data();

									if (empty($item_id))
									{
										if ($_SERVER['REQUEST_METHOD'] == "POST")
										{
												$data['msg'] = $this->inventory_model->manage();
										}

										// get inventory data
										$show_ctg = "";
										$show_entry = 10;
										$page = 1;

										if (isset($_GET['show_ctg'])) $show_ctg = str_replace("-", " ", $this->security->xss_clean($_GET['show_ctg']));
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_data($show_ctg, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_total_page($show_ctg, $show_entry);

										$this->load->view('supervisor/add_inventory_view', $data);
									}
									else if ($item_id == "search")
									{
										$keyword = "";
										$show_entry = 10;
										$page = 1;
										if (isset($_GET['keyword'])) $keyword = $this->security->xss_clean($_GET['keyword']);
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_search_data($keyword, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_search_total_page($keyword, $show_entry);
										$this->load->view('supervisor/add_inventory_view', $data);
									}
									else
									{
										if ($_SERVER['REQUEST_METHOD'] == "POST")
										{
												$data['msg'] = $this->inventory_model->manage();
										}

										$data['item'] = $this->inventory_model->get_data_by_id($item_id);
										$this->load->view('supervisor/edit_inventory_view', $data);
									}
				break;
			case 'DATAENTRY':
								// get item category
								$data['ctg_data'] = $this->inventory_model->get_ctg();
								// get user data
								$this->load->model('account_model');
								$data['user_data'] = $this->account_model->get_data();
								if ($item_id == "search")
								{
									$keyword = "";
									$show_entry = 10;
									$page = 1;
									if (isset($_GET['keyword'])) $keyword = $this->security->xss_clean($_GET['keyword']);
									if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
									if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

									$item = $this->inventory_model->get_search_data($keyword, $show_entry, $page);
									$data['item'] = $item['item_data'];
									$data['n_item'] = $item['n_data'];
									$data['n_page'] = $this->inventory_model->count_search_total_page($keyword, $show_entry);
									$this->load->view('data_entry/add_inventory_view', $data);
								}
								else
								{
									if ($_SERVER['REQUEST_METHOD'] == "POST")
									{
											$data['msg'] = $this->inventory_model->manage();
									}

									// get inventory data
									$show_ctg = "";
									$show_entry = 10;
									$page = 1;

									if (isset($_GET['show_ctg'])) $show_ctg = str_replace("-", " ", $this->security->xss_clean($_GET['show_ctg']));
									if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
									if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

									$item = $this->inventory_model->get_data($show_ctg, $show_entry, $page);
									$data['item'] = $item['item_data'];
									$data['n_item'] = $item['n_data'];
									$data['n_page'] = $this->inventory_model->count_total_page($show_ctg, $show_entry);

									$this->load->view('data_entry/add_inventory_view', $data);
								}
				;break;
			case 'VIEWER':
									// get item category
									$data['ctg_data'] = $this->inventory_model->get_ctg();
									if ($item_id == "search")
									{
										$keyword = "";
										$show_entry = 10;
										$page = 1;
										if (isset($_GET['keyword'])) $keyword = $this->security->xss_clean($_GET['keyword']);
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_search_data($keyword, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_search_total_page($keyword, $show_entry);
										$this->load->view('viewer/inventory_view', $data);
									}
									else
									{
										if ($_SERVER['REQUEST_METHOD'] == "POST")
										{
												$data['msg'] = $this->inventory_model->manage();
										}

										// get inventory data
										$show_ctg = "";
										$show_entry = 10;
										$page = 1;

										if (isset($_GET['show_ctg'])) $show_ctg = str_replace("-", " ", $this->security->xss_clean($_GET['show_ctg']));
										if (isset($_GET['show_entry'])) $show_entry = $this->security->xss_clean($_GET['show_entry']);
										if (isset($_GET['pg'])) $page = $this->security->xss_clean($_GET['pg']);

										$item = $this->inventory_model->get_data($show_ctg, $show_entry, $page);
										$data['item'] = $item['item_data'];
										$data['n_item'] = $item['n_data'];
										$data['n_page'] = $this->inventory_model->count_total_page($show_ctg, $show_entry);
										$this->load->view('viewer/inventory_view', $data);
									}
					;break;
			default:
				# code...
				break;
		}
	}

	public function category()
	{
		// check session first
		if (!$this->check_session()) redirect('dashboard/login');
		// get user role
		$role = $this->session->userdata('user_role');
		if ($role != "ADMIN" and $role != "SUPERVISOR") redirect ('dashboard/inventory');

		// load inventory model
		$this->load->model('inventory_model');
		// set default message to null
		$data['msg'] = "";

		if ($_SERVER['REQUEST_METHOD'] == "POST")
		{
			$pcs = $this->input->post('pcs');
			if ($pcs == "add" && !$this->inventory_model->check_ctg())
				$data['msg'] = "Kategori tersebut sudah ada. Tidak dapat membuat kategori dengan nama yang sama.";
			else
				$data['msg'] = $this->inventory_model->manage_category();
		}

		// get category data
		$data['ctg_data'] = $this->inventory_model->get_ctg();

		if ($role == "ADMIN")
			$this->load->view('admin/category_view', $data);
		else
			$this->load->view('supervisor/category_view', $data);
	}

	public function account()
	{
		// check session first
		if (!$this->check_session()) redirect('dashboard/login');
		// get user role
		$role = $this->session->userdata('user_role');
		if ($role != "ADMIN") redirect ('dashboard/inventory');

		// load account model
		$this->load->model('account_model');
		// set default message to null
		$data['msg'] = "";
		// get item id
		$account_id = $this->uri->segment(3);

		if (empty($account_id))
		{
			if ($_SERVER['REQUEST_METHOD'] == "POST")
	    {
	        $data['msg'] = $this->account_model->manage();
	    }

			// get inventory data
			$data['account'] = $this->account_model->get_data();

	    $this->load->view('admin/add_user_view', $data);
		}
		else
		{
			if ($_SERVER['REQUEST_METHOD'] == "POST")
	    {
	        $data['msg'] = $this->account_model->manage();
	    }

			$data['account'] = $this->account_model->get_data_by_id($account_id);
      $this->load->view('admin/edit_user_view', $data);
		}
	}

	public function login()
	{
		if ($_SERVER['REQUEST_METHOD'] == "POST")
		{
			$user_id = $this->security->xss_clean($this->input->post('user_id'));
			$user_pwd = $this->security->xss_clean($this->input->post('user_pwd'));

			if (empty($user_id) || empty($user_pwd))
			{
				$data['msg'] = "Username and/or password cannot be empty!";
				$this->load->view('admin/login_view', $data);
			}
			else
			{
				$validate = $this->login_model->validate_user();

				if (!$validate)
				{
					$data['msg'] = "Wrong email and/or password combination.";
					$this->load->view('admin/login_view', $data);
				}
				else
					redirect('dashboard/inventory');
			}
		}
		else
		{
			$data['msg'] = "";
			$this->load->view('admin/login_view', $data);
		}
	}

	private function check_session()
	{
		if (!$this->session->userdata('dashboard_access')) return FALSE;else return TRUE;
	}

	public function logout()
	{
		$this->login_model->logout();
		$this->session->sess_destroy();
		redirect('dashboard/login');
	}

}

 ?>
