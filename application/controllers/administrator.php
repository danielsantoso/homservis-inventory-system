<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('inventory_model');
	}

  public function inventory()
	{

	}

	public function account()
	{
		$data['msg'] = "";
		$action = $this->uri->segment(4);

		if (!empty($action))	// THIS IS FOR EDIT
		{

		}
		else // THIS IS FOR ADD, DELETE
		{
			if ($_SERVER['REQUEST_METHOD'] == "POST")
			{
					$this->account_model->manage();
			}

			$this->load->view('administrator/add_account_view', $data);
		}
	}
}

 ?>
