<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dropzone extends CI_Controller {

    public function __construct() {
       parent::__construct();
       $this->load->helper(array('url','html','form'));
    }

    public function index() {
        $this->load->view('dropzone_view');
    }

    public function upload_item_photo() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
    //    $fileName = $_FILES['file']['name'];
        $fileName = $_POST['fileName'];

        // Check monthly directory first - THIS IS FOR ITEM PHOTO
        if (!file_exists(getcwd() . '/uploads/inv/item_photo/original/'.date('Y'))) {
            $year = getcwd() . '/uploads/inv/item_photo/original/'.date('Y');
            mkdir($year, 0777, true);
            if (!file_exists($year.'/'.date('m'))) {
                $month = $year.'/'.date('m');
                mkdir($month, 0777, true);
            }
            else
            {
                $month = $year.'/'.date('m');
            }
        }
        else
        {
          $year = getcwd() . '/uploads/inv/item_photo/original/'.date('Y');
          $month = $year.'/'.date('m');

          if (!file_exists($year.'/'.date('m'))) {
              $month = $year.'/'.date('m');
              mkdir($month, 0777, true);
          }
          else
          {
              $month = $year.'/'.date('m');
          }
        }

        $originalPath = $month;

        $targetFile = $originalPath .'/'. $fileName;
        $upload_success = move_uploaded_file($tempFile, $targetFile);

        // RESIZED FOLDER
        if (!file_exists(getcwd() . '/uploads/inv/item_photo/resized/'.date('Y'))) {
            $year = getcwd() . '/uploads/inv/item_photo/resized/'.date('Y');
            mkdir($year, 0777, true);
            if (!file_exists($year.'/'.date('m'))) {
                $month = $year.'/'.date('m');
                mkdir($month, 0777, true);
            }
            else
            {
                $month = $year.'/'.date('m');
            }
        }
        else
        {
          $year = getcwd() . '/uploads/inv/item_photo/resized/'.date('Y');
          $month = $year.'/'.date('m');

          if (!file_exists($year.'/'.date('m'))) {
              $month = $year.'/'.date('m');
              mkdir($month, 0777, true);
          }
          else
          {
              $month = $year.'/'.date('m');
          }
        }

        $resizedPath = $month;

        $this->load->library('image_lib');

				// Resize the photo
				$config = array(
				'image_library'	  => 'gd2',
				'source_image'	  =>	$targetFile, // path to the upload image
				'new_image'		    =>	$resizedPath, //path to
				'maintain_ratio'  => TRUE,
        'width'           => 300
      	);

    		$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

        $success_message = array(
                              'success' => 200,
                              'filename' => $fileName,
                            );

        if($upload_success) {
            echo json_encode($success_message);
        } else {
            return Response::json('error', 400);
        }
        // if you want to save in db,where here
        // with out model just for example
        // $this->load->database(); // load database
        // $this->db->insert('file_table',array('file_name' => $fileName));
        }
    }

    public function upload_bill_photo() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
    //    $fileName = $_FILES['file']['name'];
        $fileName = $_POST['fileName'];

        // Check monthly directory first - THIS IS FOR ITEM PHOTO
        if (!file_exists(getcwd() . '/uploads/inv/item_bill/original/'.date('Y'))) {
            $year = getcwd() . '/uploads/inv/item_bill/original/'.date('Y');
            mkdir($year, 0777, true);
            if (!file_exists($year.'/'.date('m'))) {
                $month = $year.'/'.date('m');
                mkdir($month, 0777, true);
            }
            else
            {
                $month = $year.'/'.date('m');
            }
        }
        else
        {
          $year = getcwd() . '/uploads/inv/item_bill/original/'.date('Y');
          $month = $year.'/'.date('m');

          if (!file_exists($year.'/'.date('m'))) {
              $month = $year.'/'.date('m');
              mkdir($month, 0777, true);
          }
          else
          {
              $month = $year.'/'.date('m');
          }
        }

        $originalPath = $month;

        $targetFile = $originalPath .'/'. $fileName;

        $upload_success = move_uploaded_file($tempFile, $targetFile);

        // RESIZED FOLDER
        if (!file_exists(getcwd() . '/uploads/inv/item_bill/resized/'.date('Y'))) {
            $year = getcwd() . '/uploads/inv/item_bill/resized/'.date('Y');
            mkdir($year, 0777, true);
            if (!file_exists($year.'/'.date('m'))) {
                $month = $year.'/'.date('m');
                mkdir($month, 0777, true);
            }
            else
            {
                $month = $year.'/'.date('m');
            }
        }
        else
        {
          $year = getcwd() . '/uploads/inv/item_bill/resized/'.date('Y');
          $month = $year.'/'.date('m');

          if (!file_exists($year.'/'.date('m'))) {
              $month = $year.'/'.date('m');
              mkdir($month, 0777, true);
          }
          else
          {
              $month = $year.'/'.date('m');
          }
        }

        $resizedPath = $month;

        $this->load->library('image_lib');

				// Resize the photo
				$config = array(
				'image_library'	  => 'gd2',
				'source_image'	  =>	$targetFile, // path to the upload image
				'new_image'		    =>	$resizedPath, //path to
				'maintain_ratio'  => TRUE,
        'width'           => 150
      	);

    		$this->image_lib->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();

        $success_message = array(
                              'success' => 200,
                              'filename' => $fileName,
                            );

        if($upload_success) {
            echo json_encode($success_message);
        } else {
            return Response::json('error', 400);
        }
        // if you want to save in db,where here
        // with out model just for example
        // $this->load->database(); // load database
        // $this->db->insert('file_table',array('file_name' => $fileName));
        }
    }
}

/* End of file dropzone.js */
/* Location: ./application/controllers/dropzone.php */
