
<?php


//header("Content-type: text/csv");
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=hasil.xlsx");
//header("Content-type: application/vnd-ms-excel");

?>

<html>
<head>
  <style>
    th,td{
      text-align: center;
    }
    td{
      border:solid 1px #ccc;
      padding: 10px;
      vertical-align: middle;
      min-height: 100%;
      height:100%;
      line-height: 0px;
    }
    tr{
      min-height: 100%;
      height:100%;
    }
    img{
      width: 150px;
      height:150px;
    }
  </style>
</head>
<body>
<table>
  <thead class="">
    <tr>
    <th>No.</th>
    <th width="150">Foto</th>
    <th>Nama Barang</th>
    <th>Kategori</th>
    <th style="width:13%">Deskripsi</th>
    <th>Tanggal <br />Pembelian</th>
    <th>Nomor Voucher</th>
    <th>ID</th>
    <th>QRCode</th>
    <th>Status</th>
    <th style="width:13%">Keterangan</th>
    <th>Pemakai</th>
    <th width="150">Bukti Pembelian</th>
    </tr>
  </thead>

  <tbody>
    <?php $no=1;foreach ($item as $it) { ?>
    <tr>
      <td><?php echo $no ?></td>
      <td>
        <img width="150" height="150"
             src="<?php if ($it['ITEM_PHOTO'] == '')echo site_url('assets/template/icon/image-unavailable.png');else echo site_url('uploads/inv/item_photo/resized/'.$it['ITEM_PHOTO']) ?>">
      </td>
      <td><?php echo $it['ITEM_NAME'] ?></td>
      <td><?php echo $it['ITEM_CTG'] ?></td>
      <td style="width:13%;text-align:right"><?php echo $it['ITEM_DESC'] ?></td>
      <td><?php echo $it['ITEM_BUY_DATE'] ?></td>
      <td><?php echo $it['ITEM_VOUCHER_NO'] ?></td>
      <td><?php echo $it['ITEM_ID'] ?></td>
      <td><?php echo $it['ITEM_QRCODE'] ?></td>
      <td><?php echo $it['ITEM_STATUS'] ?></td>
      <td style="width:13%;text-align:right"><?php echo $it['ITEM_INFO'] ?></td>
      <td>
        <?php
          $it_user = str_replace(" ", "<br />", $it['ITEM_USER']);
          echo $it_user;
        ?>
      </td>
      <td>
        <img width="150" height="150"
              src="<?php if ($it['ITEM_BILL'] == '')echo site_url('assets/template/icon/image-unavailable.png');else echo site_url('uploads/inv/item_bill/resized/'.$it['ITEM_BILL']) ?>">
      </td>
    </tr>
    <?php $no++; } ?>
  </tbody>
</table>
</body>
</html>
