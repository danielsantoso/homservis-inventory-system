<?php

/** PHPExcel */
require_once (getcwd() .'/assets/PHPExcel-1.8/Classes/PHPExcel.php');

// Create new PHPExcel object
$object = new PHPExcel();

// Set properties
$object->getProperties()->setCreator("Tempo")
               ->setLastModifiedBy("Tempo")
               ->setCategory("Approve by ");
// Add some data
$object->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$object->getActiveSheet()->getColumnDimension('B')->setWidth(30);
$object->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$object->getActiveSheet()->getColumnDimension('D')->setWidth(40);
$object->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$object->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$object->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$object->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$object->getActiveSheet()->getColumnDimension('I')->setWidth(40);
$object->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$object->getActiveSheet()->mergeCells('A1:F1');
$object->getActiveSheet()->mergeCells('A2:F2');
$object->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Homservis Asset')
            ->setCellValue('A4', 'No.')
            ->setCellValue('B4', 'Foto')
            ->setCellValue('C4', 'Nama Barang')
            ->setCellValue('D4', 'Kategori')
            ->setCellValue('E4', 'Deskripsi')
            ->setCellValue('F4', 'Tanggal Pembelian')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'QRCode')
            ->setCellValue('I4', 'Status')
            ->setCellValue('J4', 'Keterangan')
            ->setCellValue('K4', 'Pemakai')
            ->setCellValue('L4', 'Bukti Pembelian');
//$object->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
/*foreach($object->getActiveSheet()->getRowDimensions() as $rd) {
    $rd->setRowHeight(-1);
}*/
$object->getActiveSheet()->getRowDimension(1)->setRowHeight(-5);

//add data
$counter=5;
$ex = $object->setActiveSheetIndex(0);
?>
<?php
  $no=1;foreach ($item as $it)
  {
    $ex->setCellValue("A".$counter, $no);
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
//    $objDrawing->setImageResource(imagecreatefromjpeg(site_url('assets/template/icon/landak.jpg')));
    $objDrawing->setImageResource(imagecreatefromjpeg(site_url('assets/template/icon/landak.jpg')));
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setHeight(150);
    $objDrawing->setCoordinates('B'.$counter);
    $objDrawing->setWorksheet($object->getActiveSheet());
    $ex->setCellValue("C".$counter, $it['ITEM_NAME']);
    $ex->setCellValue("D".$counter, $it['ITEM_CTG']);
    $ex->setCellValue("E".$counter, str_replace("\n", ' ', $it['ITEM_DESC']));
    $ex->setCellValue("F".$counter, $it['ITEM_BUY_DATE']);
    $ex->setCellValue("G".$counter, $it['ITEM_ID']);
    $ex->setCellValue("H".$counter, $it['ITEM_QRCODE']);
    $ex->setCellValue("I".$counter, $it['ITEM_STATUS']);
    $ex->setCellValue("J".$counter, $it['ITEM_INFO']);
    $ex->setCellValue("K".$counter, $it['ITEM_USER']);
    $ex->setCellValue("L".$counter, $it['ITEM_INFO']);
    $counter=$counter+1;
    $no++;
  }

  // Rename sheet
$object->getActiveSheet()->setTitle('Tes');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$object->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="tes.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
$objWriter->save('php://output');
exit;
?>
