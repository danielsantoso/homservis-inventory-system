<html>
  <head>
    <title>Homservis Inventory System</title>

    <!-- Homservis Favicon -->
    <link rel="icon" href="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>" sizes="16x16" type="image/png">

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/template/materialize/css/materialize.css')?>"  media="screen,projection"/>

    <!-- Import JQuery -->
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.2.4.min.js')?>"></script>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="<?php echo site_url('assets/template/materialize/js/materialize.min.js')?>"></script>

    <!-- Homservis CSS & JS -->
    <link href="<?php echo base_url('assets/css/homservis.css'); ?>" type="text/css" rel="stylesheet" />

    <!-- For JQuery DataTable -->
    <link href="<?php echo base_url('assets/datatable/media/css/jquery.dataTables.min.css'); ?>" type="text/css" rel="stylesheet" />
    <script src="<?php echo base_url('assets/datatable/media/js/jquery.dataTables.min.js'); ?>"></script>

    <script>
      $(document).ready(function(){
        $("#show_ctg").change(function(){$("#filter-form").submit()});
        $("#show_entry").change(function(){$("#filter-form").submit()});
        $("#sort_by").change(function() {$("#filter-form").submit()});

        $('#example').DataTable( {
        "scrollY":        "500px",
        "scrollX":        "2000px",
        "scrollCollapse": true,
        "paging":         false,
        "searching":      false
      });
      })
    </script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  </head>
  <body>
    <div class="">

      <ul id="dropdown1" class="dropdown-content">
        <li><a href="<?php echo site_url('dashboard/logout') ?>">Logout</a></li>
      </ul>
      <nav>
        <div class="nav-wrapper orange lighten-1">
          <a href="#" class="brand-logo center">Homservis Inventory</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li class="active"><a href="<?php echo site_url('dashboard/inventory') ?>">Manage Inventory</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Viewer <i class="material-icons right">arrow_drop_down</i></a></li>
          </ul>
        </div>
      </nav>

      <div class="row">
        <div class="col s12 body-content">

          <div class="row">
            <div class="col s12">
              <h7>Anda login sebagai Viewer.</h7>
            </div>

            <div class="col s12">
              <ul class="tabs">
                <li class="tab col s3"><a class="active" href="#test2">View Inventory</a></li>
              </ul>
            </div>


            <div id="test2" class="col s12">
              <div class="row">
                <div class="col s12">
                  <h5>Inventory Data</h5>
                </div>

                <?php
                  if (isset($_GET['keyword'])){
                ?>
                <div class="col s12" style="text-align:center">
                  <h5>SHOWING RESULTS FOR "<?php echo $_GET['keyword'] ?>" ...</h5>
                </div>
                <?php
                  }
                ?>

                  <div class="input-field col s2">
                    <form action="<?php echo site_url('dashboard/inventory/search') ?>" method="get">
                      <input id="icon_prefix" name="keyword" type="text" class="validate">
                      <label for="icon_prefix">Type any keywords...</label>
                    </form>
                  </div>
                  <div class="col s6" style="text-align:center">
                    <ul class="pagination">

                      <?php

                        // Get ctg and entry
                        $ctg = "";
                        $entry = 10;
                        $page = 1;
                        $sort = "desc";
                        $sort_by = "";

                        // get sort
                        if (isset($_GET['sort_by']))
                        {
                          $sort = $_GET['sort_by'];
                          $sort_by = '&sort_by='.$sort;
                        }

                        if (isset($_GET['show_ctg']) && isset($_GET['show_entry']))
                        {
                          // set ctg and entry
                          $ctg = $_GET['show_ctg'];
                          $entry = $_GET['show_entry'];
                          // set current url
                          $current_url = current_url().'?show_ctg='.$ctg.'&show_entry='.$entry.$sort_by;
                        }
                        else if (isset($_GET['show_ctg']) && !isset($_GET['show_entry']))
                        {
                          // set entry
                          $ctg = $_GET['show_ctg'];
                          // set current url
                          $current_url = current_url().'?&show_ctg='.$ctg.$sort_by;
                        }
                        else if (!isset($_GET['show_ctg']) && isset($_GET['show_entry']))
                        {
                          // set entry
                          $entry = $_GET['show_entry'];
                          // set current url
                          $current_url = current_url().'?&show_entry='.$entry.$sort_by;
                        }
                        else
                        {
                          // set current url
                          if (isset($_GET['keyword']))
                          {
                            $keyword = $_GET['keyword'];
                            $current_url = current_url().'?keyword='.$keyword.$sort_by;
                          }
                          else
                          {
                            $current_url = current_url().'?'.$sort_by;
                          }
                        }

                        // get page
                        if (isset($_GET['pg'])) $page = $_GET['pg'];

                        if ($page > 1)
                        {
                          $prev_page = $page - 1;
                      ?>
                        <li class="waves-effect"><a href="<?php echo $current_url.'&pg='.$prev_page; ?>"><i class="material-icons">chevron_left</i></a></li>
                    <?php
                      }

                      if ($n_page > 1)
                      {
                        for ($pg=1; $pg<=$n_page; $pg++)
                        {
                    ?>
                          <li class="waves-effect <?php if ($pg == $page) echo " orange lighten-1 active" ?>"><a href="<?php echo $current_url.'&pg='.$pg; ?>"><?php echo $pg; ?></a></li>
                    <?php
                        }
                      }

                      if ($page < $n_page)
                      {
                        $next_page = $page+1;
                    ?>
                        <li class="waves-effect"><a href="<?php echo $current_url.'&pg='.$next_page; ?>"><i class="material-icons">chevron_right</i></a></li>
                    <?php
                      }
                    ?>

                    </ul>
                  </div>
                  <form id="filter-form" action="<?php echo site_url('dashboard/inventory'); ?>" method="get">

                    <div class="col s2" style="text-align:right">
                      <label>Show by Category</label>
                      <select name="show_ctg" id="show_ctg" class="browser-default" required>
                        <option value="all" <?php if ($ctg == "all" || $ctg == "") echo "selected"?>>All</option>
                        <?php
                          foreach ($ctg_data as $ctgy)
                          {
                            $ctg_name = strtolower(str_replace(" ", "-", $ctgy['CTG_NAME']));
                        ?>
                        <option value="<?php echo $ctg_name; ?>" <?php if ($ctg == $ctg_name && $ctg_name != "") echo "selected"?>><?php echo $ctgy['CTG_NAME'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col s1" style="text-align:right">
                      <label>Show Entry</label>
                      <select name="show_entry" id="show_entry" class="browser-default" required>
                        <option value="10" <?php if ($entry==10) echo "selected" ?>>10</option>
                        <option value="20" <?php if ($entry==20) echo "selected" ?>>20</option>
                        <option value="50" <?php if ($entry==50) echo "selected" ?>>50</option>
                      </select>
                    </div>
                    <div class="col s1" style="text-align:right">
                      <label>Sort By</label>
                      <select name="sort_by" id="sort_by" class="browser-default" required>
                        <option value="desc" <?php if ($sort=="desc") echo "selected" ?>>Terbaru</option>
                        <option value="asc" <?php if ($sort=="asc") echo "selected" ?>>Terlama</option>
                      </select>
                    </div>
                  </form>

                  <div class="divider"></div>
                  <div class="col s12">
                    <table class="data-table striped" id="example">
                      <thead>
                        <tr>
                        <th>No.</th>
                        <th>Foto</th>
                        <th>Nama Barang</th>
                        <th>Kategori</th>
                        <th style="width:13%">Deskripsi</th>
                        <th>Tanggal <br />Pembelian</th>
                        <th>Nomor Voucher</th>
                        <th>ID</th>
                        <th>QRCode</th>
                        <th>Status</th>
                        <th style="width:13%">Keterangan</th>
                        <th>Pemakai</th>
                        <th>Bukti Pembelian</th>
                      </tr>
                      </thead>

                      <tbody>
                      <?php if ($page == 1) $no = 1;else $no = ($page-1)*$entry+1; foreach ($item as $it) { ?>
                      <tr>
                        <td><?php echo $no ?></td>
                        <td>
                          <img class="img-small materialboxed"
                               src="<?php if ($it['ITEM_PHOTO'] == '')echo site_url('assets/template/icon/image-unavailable.png');else echo site_url('uploads/inv/item_photo/resized/'.$it['ITEM_PHOTO']) ?>"/>
                        </td>
                        <td><?php echo $it['ITEM_NAME'] ?></td>
                        <td><?php echo $it['ITEM_CTG'] ?></td>
                        <td style="width:13%;text-align:right"><?php echo $it['ITEM_DESC'] ?></td>
                        <td><?php echo $it['ITEM_BUY_DATE'] ?></td>
                        <td><?php echo $it['ITEM_VOUCHER_NO'] ?></td>
                        <td><?php echo $it['ITEM_ID'] ?></td>
                        <td><?php echo $it['ITEM_QRCODE'] ?></td>
                        <td><?php echo $it['ITEM_STATUS'] ?></td>
                        <td style="width:13%;text-align:right"><?php echo $it['ITEM_INFO'] ?></td>
                        <td>
                          <?php
                            $it_user = str_replace(" ", "<br />", $it['ITEM_USER']);
                            echo $it_user;
                          ?>
                        </td>
                        <td>
                          <img class="img-small materialboxed"
                                src="<?php if ($it['ITEM_BILL'] == '')echo site_url('assets/template/icon/image-unavailable.png');else echo site_url('uploads/inv/item_bill/resized/'.$it['ITEM_BILL']) ?>">
                        </td>
                      </tr>
                      <?php $no++; } ?>
                    </tbody>
                    </table>
                    <p>
                      <?php echo $msg; ?>
                    </p>
                  </div>
              </div>


              </div>


            </div>
          </div>

        </div>
      </div>


    </div>
  </body>
</html>
