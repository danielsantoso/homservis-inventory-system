<html>
<head>
	<title>Homservis Inventory System</title>

	<!-- Homservis Favicon -->
	<link rel="icon" href="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>" sizes="16x16" type="image/png">

	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/template/materialize/css/materialize.css')?>"  media="screen,projection"/>

	<!-- Import JQuery -->
	<script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.2.4.min.js')?>"></script>

	<!--Import jQuery before materialize.js-->
	<script type="text/javascript" src="<?php echo site_url('assets/template/materialize/js/materialize.min.js')?>"></script>

	<style>
		.logo{
			text-align: center;
			margin-top: 100px;
		}
		.logo img{
			float:left;
			width: 120px;
			height: auto;
			-moz-box-shadow: 1px 2px 3px rgba(0,0,0,.5);
			-webkit-box-shadow: 1px 2px 3px rgba(0,0,0,.5);
			box-shadow: 1px 2px 3px rgba(0,0,0,.5);
			border-radius: 10px;
		}
		.logo h1{
			float:left;
			font-size:38px;
			color:white;
			margin-top: 7%;
			margin-left:50px;
		}
		.form-box{
			background: white;
			border: solid 2px #ccc;
			border-radius: 5px;
		}
	</style>
</head>
<body class="orange darken-1">
	<div class="">
    <!-- Page Content goes here -->

    <div class="row">
			<div class="col s6 push-s3 logo">
				<img src="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>"/>
				<h1>Homservis Inventory System</h1>
			</div>
		</div>

	<div class="row">
	<div class="col s4 offset-s4 form-box">

	<form action="<?php echo site_url('dashboard/login'); ?>" method="post">
	<h5> Dashboard Login </h5>
	<div class="divider"></div>
	<div class="row">
		<div class="input-field col s12">
			<i class="material-icons prefix">account_circle</i>
			<input id="icon-prefix" type="text" name="user_id" class="validate" required>
			<label for="icon-prefix">User ID</label>
		</div>
		<div class="input-field col s12">
			<i class="material-icons prefix">vpn_key</i>
			<input id="icon-prefix" type="password" name="user_pwd" required>
			<label for="icon-prefix">Password</label>
		</div>
	</div>


	<button class="btn waves-effect waves-light" style="margin-left:50px" type="submit">Login</button>
	<p style="margin-left:50px">
		<?php echo $msg ?>
	</p>
	</form>
	</div>
	</div>

	</div>
</body>
</html>
