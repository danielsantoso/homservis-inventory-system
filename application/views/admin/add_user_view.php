<html>
  <head>
    <title>Admin - Homservis Inventory System</title>

    <!-- Homservis Favicon -->
    <link rel="icon" href="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>" sizes="16x16" type="image/png">

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/template/materialize/css/materialize.css')?>"  media="screen,projection"/>

    <!-- Import JQuery -->
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.2.4.min.js')?>"></script>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="<?php echo site_url('assets/template/materialize/js/materialize.min.js')?>"></script>
    <script>
      $(document).ready(function(){
        // Materialize date picker
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears:70,
          max:true,
          format: 'yyyy-mm-dd',
        });

        var $input = $('.datepicker').pickadate();
        var picker = $input.pickadate('picker');
        $('#calendar-icon').click(function(){
          event.stopPropagation();
          picker.open();
          picker.on({close:function(){
            $('#item_buy_date').val($('.datepicker').val());
          }});
        });

        // For materialize tab
        $(document).ready(function(){
          $('ul.tabs').tabs('select_tab', 'tab_id');
        });

        // disable space on user id field
        $("input#user_id").on({
          keydown: function(e) {
            if (e.which === 32)
              return false;
          },
          change: function() {
            this.value = this.value.replace(/\s/g, "");
          }
        });
      })
    </script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <style>
      .body-content{
        margin:25px 0;
      }
      .data-table{
        font-size:12px;
      }
      .tabs .indicator {
          color: #ffa726;
      }

    </style>
  </head>
  <body>
    <div class="">

      <ul id="dropdown1" class="dropdown-content">
        <li><a href="<?php echo site_url('dashboard/logout') ?>">Logout</a></li>
      </ul>
      <nav>
        <div class="nav-wrapper orange lighten-1">
          <a href="#" class="brand-logo center">Homservis Inventory</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="<?php echo site_url('dashboard/inventory') ?>">Manage Inventory</a></li>
            <li><a href="<?php echo site_url('dashboard/category') ?>">Manage Category</a></li>
            <li class="active"><a href="<?php echo site_url('dashboard/account') ?>">Manage User Account</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Admin <i class="material-icons right">arrow_drop_down</i></a></li>
          </ul>
        </div>
      </nav>

      <div class="row">
        <div class="col s12 body-content">

          <div class="fixed-action-btn vertical" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large orange lighten-1">
              <i class="material-icons">menu</i>
            </a>
            <ul>
              <li><a class="btn-floating green"><i class="material-icons">add</i></a></li>
              <li><a class="btn-floating blue"><i class="material-icons">edit</i></a></li>
              <li><a class="btn-floating red"><i class="material-icons">delete</i></a></li>
            </ul>
          </div>

          <div class="row">
            <div class="col s12">
              <ul class="tabs">
                <li class="tab col s3"><a href="#test1">Add User Account</a></li>
                <li class="tab col s3"><a class="active" href="#test2">View User Account</a></li>
              </ul>
            </div>

            <div id="test1" class="col s6 push-s3">

              <h5>Add User Account</h5>

              <form id="" action="<?php echo site_url('dashboard/account') ?>" method="post">

              <input name="pcs" type="hidden" value="add"/>

              <label>Nama User</label>
          		<input name="user_name" required>

              <label>User ID</label>
          		<input name="user_id" id="user_id" required>

              <label>User Email</label>
          		<input name="user_email" required>

              <label>User Password</label>
          		<input name="user_pwd" type="password" required>

              <label>User Phone</label>
          		<input name="user_phone" required>

              <label>User Role</label>
              <select name="user_role" class="browser-default">
                <option value="1">Supervisor</option>
                <option value="2">Data Entry</option>
                <option value="3">Viewer</option>
              </select>

              <div class="row">
                <div class="col s6">
                  <label>Tanggal Lahir</label>
                  <input name="user_dob" id="item_buy_date" type="text" placeholder="yyyy-mm-dd" required/>
                </div>
                <div class="col s6">
                  <img id="calendar-icon" src="<?php echo site_url('assets/template/icon/calendar-icon.ico') ?>" style="width:40px;height:40px;margin-top:20px"/>
                  <input type="date" class="datepicker" id="datepick" style="display:none" required/>
                </div>
              </div>

              <label>Alamat Tinggal</label>
              <textarea name="user_address" class="materialize-textarea"></textarea>

              <button class="btn waves-effect waves-light" type="submit" name="action">Add
                <i class="material-icons right">send</i>
              </button>

              <br />
              <?php echo $msg ?>
              </form>

            </div>
            <div id="test2" class="col s12">
                <h5>User Account Data</h5>
                <div class="divider"></div>
                <table class="data-table striped">
                  <th>No.</th>
                  <th>Nama User</th>
                  <th>User ID</th>
                  <th>Role</th>
                  <th>Alamat Tinggal</th>
                  <th>Tanggal Lahir</th>
                  <th>Email</th>
                  <th>Telepon</th>
                  <th>Dibuat pada</th>
                  <th>Aksi</th>

                  <?php $no = 1; foreach ($account as $acc) { ?>
                  <tr>
                    <td><?php echo $no; ?></td>
                    <td><?php echo $acc['USER_NAME'] ?></td>
                    <td><?php echo $acc['USER_ID'] ?></td>
                    <td><?php echo $acc['USER_ROLE'] ?></td>
                    <td><?php echo $acc['USER_ADDRESS'] ?></td>
                    <td><?php echo $acc['USER_DOB'] ?></td>
                    <td><?php echo $acc['USER_EMAIL'] ?></td>
                    <td><?php echo $acc['USER_PHONE'] ?></td>
                    <td><?php echo $acc['CREATE_TIME'] ?></td>
                    <td>
                      <a class="waves-effect waves-light btn" style="padding: 2px 5px;font-size:10px" href="<?php echo site_url('dashboard/account/'.$acc['USER_ID']) ?>">Edit</a>
                      <a class="waves-effect waves-light btn  modal-trigger trigger-btn" style="padding: 2px 5px;font-size:10px" href="#modal1" user-id="<?php echo $acc['USER_ID'] ?>">Hapus</a>
                    </td>
                  </tr>
                  <?php $no++; } ?>
                </table>
            </div>
          </div>

        </div>
      </div>

      <!-- Modal Structure -->
      <div id="modal1" class="modal">
        <div class="modal-content">
          <h4>Modal Header</h4>
          <p>Apakah Anda yakin ingin menghapus item ini?</p>
        </div>
        <div class="modal-footer">
          <form id="delete-form" action="<?php echo site_url('dashboard/account') ?>" method="post">
            <input name="pcs" type="hidden" value="delete"/>
            <input id="user_id_del" name="user_id" type="hidden" value=""/>
          </form>
          <button form="delete-form" class=" modal-action modal-close waves-effect waves-green btn red darken-4" style="margin:15px">Delete</button>
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn blue-grey lighten-3" style="margin:15px">Cancel</a>
        </div>
      </div>

    </div>
    <script>
      // For modal box
      $('.modal-trigger').leanModal();

      $('.trigger-btn').click(function(){
        var user_id_del = $(this).attr('user-id');
        $('#modal1').find('h4').text("User ID : "+user_id_del);
        $('#user_id_del').val(user_id_del);
      });
    </script>
  </body>
</html>
