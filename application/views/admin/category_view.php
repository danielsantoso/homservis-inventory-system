<html>
  <head>
    <title>Homservis Inventory System</title>

    <!-- Homservis Favicon -->
    <link rel="icon" href="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>" sizes="16x16" type="image/png">

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/template/materialize/css/materialize.css')?>"  media="screen,projection"/>

    <!-- Import JQuery -->
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.2.4.min.js')?>"></script>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="<?php echo site_url('assets/template/materialize/js/materialize.min.js')?>"></script>

    <!-- Dropzone -->
    <link href="<?php echo base_url('assets/dropzone-master/dist/dropzone.css'); ?>" type="text/css" rel="stylesheet" />
    <script src="<?php echo base_url('assets/dropzone-master/dist/dropzone.js'); ?>"></script>

    <!-- Homservis CSS & JS -->
    <link href="<?php echo base_url('assets/css/homservis.css'); ?>" type="text/css" rel="stylesheet" />
    <script src="<?php echo base_url('assets/js/homservis.js'); ?>"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <style>
      .data-table td{
        text-align: center;
        padding: 0px;
      }
    </style>

  </head>
  <body>
    <div class=""> <!-- Containter -->

      <ul id="dropdown1" class="dropdown-content">
        <li><a href="<?php echo site_url('dashboard/logout') ?>">Logout</a></li>
      </ul>
      <nav>
        <div class="nav-wrapper orange lighten-1">
          <a href="#" class="brand-logo center">Homservis Inventory</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="<?php echo site_url('dashboard/inventory') ?>">Manage Inventory</a></li>
            <li class="active"><a href="<?php echo site_url('dashboard/category') ?>">Manage Category</a></li>
            <li><a href="<?php echo site_url('dashboard/account') ?>">Manage User Account</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Admin <i class="material-icons right">arrow_drop_down</i></a></li>
          </ul>
        </div>
      </nav>

      <div class="row">
        <div class="col s12 body-content">

          <div class="row">

            <div class="col s3">

              <h5>Add Category</h5>

              <form id="add-form" action="<?php echo site_url('dashboard/category') ?>" method="post">

              <input name="pcs" type="hidden" value="add"/>

              <div class="row">
                <div class="col s12">
                  <label>Nama Kategori</label>
                  <input name="ctg_name" required/>
                </div>
              </div>

              <div class="row">
                <div class="col s12">
                  <button class="btn waves-effect waves-light" type="submit" name="action" form="add-form">Add Category
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
              <p>
                <?php echo $msg; ?>
              </p>

              </form>
            </div>


                <div class="col s6">
                  <h5>Category List</h5>

                    <table class="data-table striped">
                      <th>No.</th>
                      <th>ID Kategori</th>
                      <th>Nama Kategori</th>
                      <th>Slug</th>
                      <th>Aksi</th>


                      <?php $no=1;foreach ($ctg_data as $ctg) { ?>
                      <tr>
                        <td><?php echo $no ?></td>
                        <td><?php echo $ctg['CTG_ID'] ?></td>
                        <td><?php echo $ctg['CTG_NAME'] ?></td>
                        <td><?php echo strtolower(str_replace(" ", "-", $ctg['CTG_NAME'])) ?></td>
                        <td>
                          <a class="waves-effect waves-light btn modal-trigger edit-trigger-btn" style="padding: 2px 5px;font-size:10px" href="#modal2" ctg-id="<?php echo $ctg['CTG_ID'] ?>">Edit</a>
                          <a class="waves-effect waves-light btn modal-trigger delete-trigger-btn" style="padding: 2px 5px;font-size:10px" href="#modal1" ctg-id="<?php echo $ctg['CTG_ID'] ?>">Hapus</a>
                        </td>
                      </tr>
                      <?php $no++; } ?>
                    </table>


            </div>

          </div>

        </div>
      </div>

      <!-- Modal Structure -->
      <div id="modal1" class="modal">
        <div class="modal-content">
          <h4>Modal Header</h4>
          <p>Apakah Anda yakin ingin menghapus item ini?</p>
        </div>
        <div class="modal-footer">
          <form id="delete-form" action="<?php echo site_url('dashboard/category') ?>" method="post">
            <input name="pcs" type="hidden" value="delete"/>
            <input id="ctg_id_del" name="ctg_id" type="hidden" value=""/>
          </form>
          <button form="delete-form" class=" modal-action modal-close waves-effect waves-green btn red darken-4" style="margin:15px">Delete</button>
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn blue-grey lighten-3" style="margin:15px">Cancel</a>
        </div>
      </div>

      <!-- Modal Structure -->
      <div id="modal2" class="modal">
        <div class="modal-content">
          <h4>Edit Kategori</h4>
          <form id="edit-form" action="<?php echo site_url('dashboard/category') ?>" method="post">
            <input name="pcs" type="hidden" value="edit"/>
            <input id="ctg_id_edit" name="ctg_id" type="hidden" value=""/>
            <label>Nama Kategori yang baru</label>
            <input name="ctg_name" value=""/>
          </form>
        </div>
        <div class="modal-footer">
          <button form="edit-form" class=" modal-action modal-close waves-effect waves-green btn yellow darken-3" style="margin:15px">Edit</button>
          <a href="#!" class=" modal-action modal-close waves-effect waves-green btn blue-grey lighten-3" style="margin:15px">Cancel</a>
        </div>
      </div>

    </div> <!-- Containter -->

    <script>
      $('.delete-trigger-btn').click(function(){
        var ctg_id_del = $(this).attr('ctg-id');
        $('#modal1').find('h4').text("Category ID : "+ctg_id_del);
        $('#ctg_id_del').val(ctg_id_del);
      });

      $('.edit-trigger-btn').click(function(){
        var ctg_id_edit = $(this).attr('ctg-id');
        $('#ctg_id_edit').val(ctg_id_edit);
      });
    </script>
  </body>
</html>
