<html>
  <head>
    <title>Homservis Inventory System</title>

    <!-- Homservis Favicon -->
    <link rel="icon" href="<?php echo site_url('assets/template/icon/homservis-logo.png') ?>" sizes="16x16" type="image/png">

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo site_url('assets/template/materialize/css/materialize.min.css')?>"  media="screen,projection"/>

    <!-- Import JQuery -->
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-2.2.4.min.js')?>"></script>

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="<?php echo site_url('assets/template/materialize/js/materialize.min.js')?>"></script>

    <!-- Dropzone -->
    <link href="<?php echo base_url('assets/dropzone-master/dist/dropzone.css'); ?>" type="text/css" rel="stylesheet" />
    <script src="<?php echo base_url('assets/dropzone-master/dist/dropzone.js'); ?>"></script>

    <!-- Homservis CSS & JS -->
    <link href="<?php echo base_url('assets/css/homservis.css'); ?>" type="text/css" rel="stylesheet" />
    <script src="<?php echo base_url('assets/js/homservis.js'); ?>"></script>

  </head>
  <body>
    <div class="">

      <ul id="dropdown1" class="dropdown-content">
        <li><a href="<?php echo site_url('dashboard/logout') ?>">Logout</a></li>
      </ul>
      <nav>
        <div class="nav-wrapper orange lighten-1">
          <a href="#" class="brand-logo center">Homservis Inventory</a>
          <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li class="active"><a href="<?php echo site_url('dashboard/inventory') ?>">Manage Inventory</a></li>
            <li><a href="<?php echo site_url('dashboard/category') ?>">Manage Category</a></li>
            <li><a href="<?php echo site_url('dashboard/account') ?>">Manage User Account</a></li>
          </ul>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Admin <i class="material-icons right">arrow_drop_down</i></a></li>
          </ul>
        </div>
      </nav>

      <div class="row">

        <div class="col s8 body-content">

          <div class="fixed-action-btn horizontal click-to-toggle" style="bottom: 45px; right: 24px;">
            <a class="btn-floating btn-large orange lighten-1">
              <i class="material-icons">menu</i>
            </a>
            <ul>
              <li><a class="btn-floating green"><i class="material-icons">add</i></a></li>
              <li><a class="btn-floating blue"><i class="material-icons">edit</i></a></li>
              <li><a class="btn-floating red"><i class="material-icons">delete</i></a></li>
            </ul>
          </div>
        </div>


        <div class="col s6 push-s3">

            <?php foreach ($item as $it) ?>

            <form id="edit-form" action="<?php echo site_url('dashboard/inventory/'.$it['ITEM_ID']) ?>" method="post">

            <h5>Edit Inventory Data - <?php echo $it['ITEM_ID'] ?></h5>

            <img class="materialboxed img-medium"
                 data-caption="Image Unavailable"
                 src="<?php if ($it['ITEM_PHOTO'] == '')echo site_url('assets/template/icon/image-unavailable.png');else echo site_url('uploads/inv/item_photo/resized/'.$it['ITEM_PHOTO']) ?>"/>

            <input name="pcs" value="edit" type="hidden"/>

            <input name="item_id" type="hidden" value="<?php echo $it['ITEM_ID'] ?>" required/>

            <div class="row">
              <div class="col s6">
                <label>ID Barang</label>
                <input value="<?php echo $it['ITEM_ID'] ?>" disabled/>
              </div>
              <div class="col s6">
                <label>QR Code</label>
                <input name="item_qrcode" value="<?php echo $it['ITEM_QRCODE'] ?>" id="item_qrcode" required/>
              </div>
              <div class="col s6">
                <label>Nama Barang</label>
            		<input name="item_name" value="<?php echo $it['ITEM_NAME'] ?>" required>
              </div>
              <div class="col s6">
                <label>Kategori</label>
                <select name="item_ctg" class="browser-default" required>
                  <option value="">---</option>
                  <?php foreach ($ctg_data as $ctg) {?>
                  <option value="<?php echo $ctg['CTG_NAME'] ?>"><?php echo $ctg['CTG_NAME'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col s12">
                <label>Deskripsi</label>
                <textarea name="item_desc" class="materialize-textarea" required><?php echo $it['ITEM_DESC'] ?></textarea>
              </div>
            </div>

            <div class="row">
              <div class="col s6">
              <?php

                $buy_date = $it['ITEM_BUY_DATE'];
                $buy_date = explode("-", $buy_date);
                $buy_date = $buy_date[2].'-'.$buy_date[1].'-'.$buy_date[0];

              ?>
              <div class="row">
                <div class="col s6">
                  <label>Tanggal Pembelian</label>
                  <input name="item_buy_date" id="item_buy_date" type="text" value="<?php echo $buy_date ?>" placeholder="dd-mm-yyyy" required/>
                </div>
                <div class="col s6">
                  <img id="calendar-icon" src="<?php echo site_url('assets/template/icon/calendar-icon.ico') ?>" style="width:40px;height:40px;margin-top:20px"/>
                  <input type="date" class="datepicker" id="datepick" style="display:none" required/>
                </div>
              </div>
              <?php
                /*
                $buy_date = $it['ITEM_BUY_DATE'];
                $buy_date = explode("-", $buy_date);
                $month = $buy_date[1];
        				switch ($month)
        				{
        					case "01":
        						$month = "January";
        					;break;
        					case "02":
        						$month = "February";
        					;break;
        					case "03":
        						$month = "March";
        					;break;
        					case "04":
        						$month = "April";
        					;break;
        					case "05":
        						$month = "May";
        					;break;
        					case "06":
        						$month = "June";
        					;break;
        					case "07":
        						$month = "July";
        					;break;
        					case "08":
        						$month = "August";
        					;break;
        					case "09":
        						$month = "September";
        					;break;
        					case "10":
        						$month = "October";
        					;break;
        					case "11":
        						$month = "November";
        					;break;
        					case "12":
        						$month = "December";
        					;break;
        				}
                $buy_date = $buy_date[2].' '.$month.', '.$buy_date[0];
                */

              ?>

              <!--<input name="item_buy_date" type="date" class="datepicker" value="<?php echo $buy_date ?>" required>-->
            </div>
          </div>

            <div class="row">
              <div class="col s6">
                <label>Nomor Voucher</label>
                <input name="item_voucher_no" value="<?php echo $it['ITEM_VOUCHER_NO'] ?>" required/>
              </div>
              <div class="col s6">
                <label>Status Barang</label>
                <select name="item_status" class="browser-default" required>
                  <option value="">---</option>
                  <option value="1">Dikantor</option>
                  <option value="2">Tidak Dikantor</option>
                </select>
              </div>
            </div>

            <div class="row">
              <div class="col s6">
                <label>Keterangan</label>
                <textarea name="item_info" class="materialize-textarea"><?php echo $it['ITEM_INFO'] ?></textarea>
              </div>
            </div>
            <div class="row item_user_field">
              <div class="col s12">
                  <label>Pemakai</label>
              </div>
                  <?php
                    $no=0;
                    $item_user = $it['ITEM_USER'];
                    $item_user = explode(" ", $item_user);
                    $n_user = sizeof($item_user);
                    foreach ($user_data as $user) {
                  ?>
                  <div class="col s3">
                    <p>
                    <input id="item_user<?php echo $no; ?>"
                        type="checkbox"
                        name="item_user[]"
                        value="<?php echo $user['USER_ID'] ?>"
                        <?php
                          for ($i=0;$i<$n_user;$i++)
                          {
                            if ($item_user[$i] == $user['USER_ID'])
                              echo 'checked';
                          }
                        ?>>
                    <label for="item_user<?php echo $no; ?>"><?php echo $user['USER_NAME'] ?></label>
                    </p>
                  </div>
                  <?php $no++;} ?>
            </div>

            <input name="item_photo" id="item_photo" type="hidden"/>

            <input name="item_bill" id="item_bill" type="hidden"/>

            <br />

          </form>

          <form id="myDropzone" action="<?php echo site_url('dropzone/upload_item_photo'); ?>" class="dropzone"  >
            <label>Foto Barang</label>
          </form>

          <form id="myDropzone2" action="<?php echo site_url('dropzone/upload_bill_photo'); ?>" class="dropzone"  >
            <label>Foto Tanda Terima</label>
          </form>

          <button class="btn waves-effect waves-light" type="submit" name="action" form="edit-form">Save Edit
            <i class="material-icons right">send</i>
          </button>

          <p>
            <?php echo $msg ?>
          </p>
          </div>

        </div>



      </div>
  </body>
</html>
