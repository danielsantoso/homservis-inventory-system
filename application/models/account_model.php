<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function manage()
	{
		// set default message to null
		$msg = "";

		$process = $this->input->post('pcs');

		switch ($process) {
			case 'add':
				// translate user dob to date mysql format
				$user_dob = $this->input->post('user_dob');
				/*
				$user_dob = str_replace(",", "", $user_dob);
				$user_dob = str_replace(" ", "-", $user_dob);
				$user_dob = explode("-", $user_dob);
				$day = $user_dob[0];
				$month = $user_dob[1];
				switch ($month)
				{
					case "January":
						$month = "01";
					;break;
					case "February":
						$month = "02";
					;break;
					case "March":
						$month = "03";
					;break;
					case "April":
						$month = "04";
					;break;
					case "May":
						$month = "05";
					;break;
					case "June":
						$month = "06";
					;break;
					case "July":
						$month = "07";
					;break;
					case "August":
						$month = "08";
					;break;
					case "September":
						$month = "09";
					;break;
					case "October":
						$month = "10";
					;break;
					case "November":
						$month = "11";
					;break;
					case "December":
						$month = "12";
					;break;
				}
				$year = $user_dob[2];
				$user_dob = $year.'-'.$month.'-'.$day;
				*/

				$user_role = $this->input->post('user_role');
				switch ($user_role) {
					case 1:$user_role = "SUPERVISOR";break;
					case 2:$user_role = "DATAENTRY";break;
					case 3:$user_role = "VIEWER";break;
					default:
						# code...
						break;
				}

				$data = array(
					'USER_ID'				=>	$this->input->post('user_id'),
					'USER_NAME'			=>	ucwords($this->input->post('user_name')),
					'USER_PASSWORD'	=>	sha1($this->input->post('user_pwd')),
					'USER_ROLE'			=>	$user_role,
					'USER_EMAIL'		=>	$this->input->post('user_email'),
					'USER_PHONE'		=>	$this->input->post('user_phone'),
					'USER_ADDRESS'	=>	ucwords($this->input->post('user_address')),
					'USER_DOB'			=>	$user_dob
				);

				$result = $this->db->insert('user_account', $data);

				if (!$result)
					$msg = "Gagal menambahkan user baru.";
				else
					$msg = "Berhasil menambahkan user baru.";
				break;
			case 'edit':
				// get the user id
				$user_id = $this->input->post('user_id');
				// translate user dob to date mysql format
				$user_dob = $this->input->post('user_dob');
				$user_role = $this->input->post('user_role');
				switch ($user_role) {
					case 1:$user_role = "SUPERVISOR";break;
					case 2:$user_role = "DATAENTRY";break;
					case 3:$user_role = "VIEWER";break;
					default:
						# code...
						break;
				}

				$data = array(
					'USER_ID'				=>	$this->input->post('user_id'),
					'USER_NAME'			=>	ucwords($this->input->post('user_name')),
					'USER_PASSWORD'	=>	sha1($this->input->post('user_pwd')),
					'USER_ROLE'			=>	$user_role,
					'USER_EMAIL'		=>	$this->input->post('user_email'),
					'USER_PHONE'		=>	$this->input->post('user_phone'),
					'USER_ADDRESS'	=>	ucwords($this->input->post('user_address')),
					'USER_DOB'			=>	$user_dob
				);

				$this->db->where('USER_ID', $user_id);
				$result = $this->db->update('user_account', $data);

				if (!$result)
					$msg = "Gagal mengedit data user.";
				else
					$msg = "Berhasil mengedit data user.";
				break;
			case 'delete':
				$user_id = $this->input->post('user_id');
				$result = $this->db->query("UPDATE user_account SET USER_STATUS = 'INACTIVE' WHERE USER_ID = '$user_id'");

				if (!$result)
					$msg = "Gagal menghapus data.";
				else
					$msg = "Berhasil menghapus data.";
				break;
			default:
				# code...
				break;
		}

		return $msg;
	}

	public function get_data()
	{
		$this->db->where('USER_STATUS', 'ACTIVE');
		$data = $this->db->get('user_account');
		return $data->result_array();
	}

	public function get_data_by_id($id)
	{
		$this->db->where('USER_STATUS', 'ACTIVE');
		$this->db->where('USER_ID', $id);
		$data = $this->db->get('user_account');
		return $data->result_array();
	}
}

?>
