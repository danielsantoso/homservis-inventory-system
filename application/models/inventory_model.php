<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function manage()
	{
		// set default message to null
		$msg = "";
		// get the process
		$process = $this->input->post('pcs');

		switch ($process) {
			case 'add':
				$item_id = strtoupper($this->input->post('item_id'));
				$item_id = trim($item_id);
				$item_id = str_replace(" ", "_", $item_id);
				$item_qrcode = trim(strtoupper($this->input->post('item_qrcode')));
				$item_name = ucwords($this->input->post('item_name'));
				$item_desc = ucfirst($this->input->post('item_desc'));
				$item_voucher_no = strtoupper($this->input->post('item_voucher_no'));
				$item_info = ucwords($this->input->post('item_info'));
				$item_user_temp = $_POST['item_user'];
				$item_user = "";
				foreach ($item_user_temp as $iu)
				{
					if ($item_user == "")
						$item_user = $iu;
					else
					 	$item_user .= ' '.$iu;
				}
				$item_photo = $this->input->post('item_photo');
				$item_bill = $this->input->post('item_bill');
				$buy_date = $this->input->post('item_buy_date');
				$buy_date = explode('-', $buy_date);
				$buy_date = $buy_date[2].'-'.$buy_date[1].'-'.$buy_date[0];
				/*
				$buy_date = str_replace(",", "", $buy_date);
				$buy_date = str_replace(" ", "-", $buy_date);
				$buy_date = explode("-", $buy_date);
				$day = $buy_date[0];
				$month = $buy_date[1];
				switch ($month)
				{
					case "January":
						$month = "01";
					;break;
					case "February":
						$month = "02";
					;break;
					case "March":
						$month = "03";
					;break;
					case "April":
						$month = "04";
					;break;
					case "May":
						$month = "05";
					;break;
					case "June":
						$month = "06";
					;break;
					case "July":
						$month = "07";
					;break;
					case "August":
						$month = "08";
					;break;
					case "September":
						$month = "09";
					;break;
					case "October":
						$month = "10";
					;break;
					case "November":
						$month = "11";
					;break;
					case "December":
						$month = "12";
					;break;
				}
				$year = $buy_date[2];
				$buy_date = $year.'-'.$month.'-'.$day;
				*/

				// item ctg
				$item_ctg = $this->input->post('item_ctg');
				/*
				switch ($item_ctg)
				{
					case 1:	$item_ctg = "PC";break;
					case 2:	$item_ctg = "Laptop";break;
					case 3:	$item_ctg = "TV";break;
					case 4:	$item_ctg = "Printer";break;
					case 5:	$item_ctg = "Keyboard";break;
					case 6:	$item_ctg = "Speaker";break;
				}
				*/
				// item status
				$item_status = $this->input->post('item_status');
				switch ($item_status)
				{
					case 1:$item_status = "DIKANTOR";break;
					case 1:$item_status = "TIDAK DIKANTOR";break;
				}

				if ($item_photo == "")
				{
					$data = array(
									'ITEM_ID'						=>	$item_id,
									'ITEM_QRCODE'				=>	$item_qrcode,
									'ITEM_NAME'					=>	$item_name,
									'ITEM_CTG'					=>	$item_ctg,
									'ITEM_DESC'					=>	$item_desc,
									'ITEM_BUY_DATE'			=>	$buy_date,
									'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
									'ITEM_STATUS'				=>	$item_status,
									'ITEM_INFO'					=>	$item_info,
									'ITEM_USER'					=>	$item_user,
									'ITEM_BILL'					=>	$item_bill
					);
				}
				else
				{
					$data = array(
									'ITEM_ID'						=>	$item_id,
									'ITEM_QRCODE'				=>	$item_qrcode,
									'ITEM_NAME'					=>	$item_name,
									'ITEM_CTG'					=>	$item_ctg,
									'ITEM_DESC'					=>	$item_desc,
									'ITEM_BUY_DATE'			=>	$buy_date,
									'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
									'ITEM_STATUS'				=>	$item_status,
									'ITEM_INFO'					=>	$item_info,
									'ITEM_USER'					=>	$item_user,
									'ITEM_PHOTO'				=>	date('Y').'/'.date('m').'/'.$item_photo,
									'ITEM_BILL'					=>	$item_bill
					);
				}

				$result = $this->db->insert('inventory', $data);

				if (!$result)
					$msg = "Gagal menambah data.";
				else
					$msg = "Berhasil menambah data.";

				break;
			case 'edit':
				$item_id = $this->input->post('item_id');
				$item_qrcode = trim(strtoupper($this->input->post('item_qrcode')));
				$item_name = ucwords($this->input->post('item_name'));
				$item_desc = ucfirst($this->input->post('item_desc'));
				$item_voucher_no = strtoupper($this->input->post('item_voucher_no'));
				$item_info = ucwords($this->input->post('item_info'));
				$item_user_temp = $_POST['item_user'];
				$item_user = "";
				foreach ($item_user_temp as $iu)
				{
					if ($item_user == "")
						$item_user = $iu;
					else
					 	$item_user .= ' '.$iu;
				}
				$item_photo = $this->input->post('item_photo');
				$item_bill = $this->input->post('item_bill');
				$buy_date = $this->input->post('item_buy_date');
				$buy_date = explode('-', $buy_date);
				$buy_date = $buy_date[2].'-'.$buy_date[1].'-'.$buy_date[0];
				/*
				$buy_date = str_replace(",", "", $buy_date);
				$buy_date = str_replace(" ", "-", $buy_date);
				$buy_date = explode("-", $buy_date);
				$day = $buy_date[0];
				$month = $buy_date[1];
				switch ($month)
				{
					case "January":
						$month = "01";
					;break;
					case "February":
						$month = "02";
					;break;
					case "March":
						$month = "03";
					;break;
					case "April":
						$month = "04";
					;break;
					case "May":
						$month = "05";
					;break;
					case "June":
						$month = "06";
					;break;
					case "July":
						$month = "07";
					;break;
					case "August":
						$month = "08";
					;break;
					case "September":
						$month = "09";
					;break;
					case "October":
						$month = "10";
					;break;
					case "November":
						$month = "11";
					;break;
					case "December":
						$month = "12";
					;break;
				}
				$year = $buy_date[2];
				$buy_date = $year.'-'.$month.'-'.$day;
				*/

				// item ctg
				$item_ctg = $this->input->post('item_ctg');
				/*
				switch ($item_ctg)
				{
					case 1:	$item_ctg = "PC";break;
					case 2:	$item_ctg = "Laptop";break;
					case 3:	$item_ctg = "TV";break;
					case 4:	$item_ctg = "Printer";break;
					case 5:	$item_ctg = "Keyboard";break;
					case 6:	$item_ctg = "Speaker";break;
				}
				*/
				// item status
				$item_status = $this->input->post('item_status');
				switch ($item_status)
				{
					case 1:$item_status = "DIKANTOR";break;
					case 1:$item_status = "TIDAK DIKANTOR";break;
				}

				if (empty($item_photo))
				{
					if (empty($item_bill))
					{
						$data = array(
										'ITEM_QRCODE'				=>	$item_qrcode,
										'ITEM_NAME'					=>	$item_name,
										'ITEM_CTG'					=>	$item_ctg,
										'ITEM_DESC'					=>	$item_desc,
										'ITEM_BUY_DATE'			=>	$buy_date,
										'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
										'ITEM_STATUS'				=>	$item_status,
										'ITEM_INFO'					=>	$item_info,
										'ITEM_USER'					=>	$item_user,
						);
					}
					else
					{
						$data = array(
										'ITEM_QRCODE'				=>	$item_qrcode,
										'ITEM_NAME'					=>	$item_name,
										'ITEM_CTG'					=>	$item_ctg,
										'ITEM_DESC'					=>	$item_desc,
										'ITEM_BUY_DATE'			=>	$buy_date,
										'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
										'ITEM_STATUS'				=>	$item_status,
										'ITEM_INFO'					=>	$item_info,
										'ITEM_USER'					=>	$item_user,
										'ITEM_BILL'					=>	date('Y').'/'.date('m').'/'.$item_bill
						);
					}
				}
				else
				{
					if (empty($item_bill))
					{
						$data = array(
										'ITEM_QRCODE'				=>	$item_qrcode,
										'ITEM_NAME'					=>	$item_name,
										'ITEM_CTG'					=>	$item_ctg,
										'ITEM_DESC'					=>	$item_desc,
										'ITEM_BUY_DATE'			=>	$buy_date,
										'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
										'ITEM_STATUS'				=>	$item_status,
										'ITEM_INFO'					=>	$item_info,
										'ITEM_USER'					=>	$item_user,
										'ITEM_PHOTO'				=>	date('Y').'/'.date('m').'/'.$item_photo,
						);
					}
					else
					{
						$data = array(
										'ITEM_QRCODE'				=>	$item_qrcode,
										'ITEM_NAME'					=>	$item_name,
										'ITEM_CTG'					=>	$item_ctg,
										'ITEM_DESC'					=>	$item_desc,
										'ITEM_BUY_DATE'			=>	$buy_date,
										'ITEM_VOUCHER_NO'		=>	$item_voucher_no,
										'ITEM_STATUS'				=>	$item_status,
										'ITEM_INFO'					=>	$item_info,
										'ITEM_USER'					=>	$item_user,
										'ITEM_PHOTO'				=>	date('Y').'/'.date('m').'/'.$item_photo,
										'ITEM_BILL'					=>	date('Y').'/'.date('m').'/'.$item_bill
							);
					}
				}

				$this->db->where('ITEM_ID', $item_id);
				$result = $this->db->update('inventory', $data);

				if (!$result)
					$msg = "Gagal mengedit data.";
				else
					$msg = "Berhasil mengedit data.";

				break;
			case 'delete':
				$item_id = $this->input->post('item_id');
				$result = $this->db->query("UPDATE inventory SET ITEM_ACTIVE = 'N' WHERE ITEM_ID = '$item_id'");

				if (!$result)
					$msg = "Gagal menghapus data.";
				else
					$msg = "Berhasil menghapus data.";
				break;
			default:
				# code...
				break;
		}

		return $msg;
	}

	public function count_total_page($ctg, $entry)
	{
		if ($ctg == "All")
		{
			$query = "SELECT * FROM inventory WHERE ITEM_ACTIVE = 'Y'";
		}
		else if ($ctg != "")
		{
				$query = "SELECT * FROM inventory WHERE ITEM_CTG = '$ctg' AND ITEM_ACTIVE = 'Y'";
		}
		else
		{
			$query = "SELECT * FROM inventory WHERE ITEM_ACTIVE = 'Y'";
		}

		$result = $this->db->query($query);

		$n_item = $result->num_rows();

		$n_page = ceil($n_item / $entry);

		return $n_page;
	}

	public function get_data($ctg, $entry, $pg, $sort)
	{
		// Get sort type
		if ($sort == "desc") $sort = "DESC";else $sort = "ASC";

		if ($pg > 1)
		{
			$start = ($pg - 1) * $entry;

			if ($ctg == "All")
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT $start, $entry";
			}
			else if ($ctg != "" && $entry != "")
			{
					$query = "SELECT * FROM inventory AS inv
												INNER JOIN inventory_ctg  AS inv_ctg
												ON inv.ITEM_CTG = inv_ctg.CTG_NAME
												WHERE ITEM_CTG = '$ctg'
												AND ITEM_ACTIVE = 'Y'
												ORDER BY ITEM_BUY_DATE
												$sort LIMIT $start, $entry";
			}
			else if ($ctg != "")
			{
				$query = "SELECT * FROM inventory AS inv
												INNER JOIN inventory_ctg  AS inv_ctg
												ON inv.ITEM_CTG = inv_ctg.CTG_NAME
												WHERE ITEM_CTG = '$ctg'
												AND ITEM_ACTIVE = 'Y'
												ORDER BY ITEM_BUY_DATE
												$sort LIMIT $start, 10";
			}
			else if ($entry != "")
			{
				$query = "SELECT * FROM inventory AS inv
												INNER JOIN inventory_ctg  AS inv_ctg
												ON inv.ITEM_CTG = inv_ctg.CTG_NAME
												WHERE ITEM_ACTIVE = 'Y'
												ORDER BY ITEM_BUY_DATE
												$sort LIMIT $start, $entry";
			}
			else
			{
				$query = "SELECT * FROM inventory AS inv
												INNER JOIN inventory_ctg  AS inv_ctg
												ON inv.ITEM_CTG = inv_ctg.CTG_NAME
												WHERE ITEM_ACTIVE = 'Y'
												ORDER BY ITEM_BUY_DATE
												$sort LIMIT $start, 10";
			}
		}
		else
		{
			if ($ctg == "All")
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT 0, $entry";
			}
			else if ($ctg != "" && $entry != "")
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_CTG = '$ctg'
											AND ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT 0, $entry";
			}
			else if ($ctg != "")
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_CTG = '$ctg'
											AND ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT 0, 10";
			}
			else if ($entry != "")
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT 0, $entry";
			}
			else
			{
				$query = "SELECT * FROM inventory AS inv
											INNER JOIN inventory_ctg  AS inv_ctg
											ON inv.ITEM_CTG = inv_ctg.CTG_NAME
											WHERE ITEM_ACTIVE = 'Y'
											ORDER BY ITEM_BUY_DATE
											$sort LIMIT 0, 10";
			}
		}

		$result = $this->db->query($query);
		$data = $result->result_array();
		$n_data = $result->num_rows();

		return array('item_data' => $data, 'n_data' => $n_data);
	}

	public function count_search_total_page($kw, $entry)
	{
		$query = "SELECT * FROM inventory WHERE ITEM_NAME LIKE '%$kw%' AND ITEM_ACTIVE = 'Y'";

		$result = $this->db->query($query);

		$n_item = $result->num_rows();

		$n_page = ceil($n_item / $entry);

		return $n_page;
	}

	public function get_search_data($kw, $entry, $pg, $sort)
	{
		// Get sort type
		if ($sort == "desc") $sort = "DESC";else $sort = "ASC";

		if ($pg > 1)
		{
			$start = ($pg - 1) * $entry;

			if ($entry != "")
			{
				$query = "SELECT * FROM inventory WHERE ITEM_NAME LIKE '%$kw%' ORDER BY ITEM_BUY_DATE $sort LIMIT $start, $entry";
			}
			else
			{
				$query = "SELECT * FROM inventory WHERE ITEM_NAME LIKE '%$kw%' ORDER BY ITEM_BUY_DATE $sort LIMIT $start, 10";
			}
		}
		else
		{
			if ($entry != "")
			{
				$query = "SELECT * FROM inventory WHERE ITEM_NAME LIKE '%$kw%' ORDER BY ITEM_BUY_DATE $sort LIMIT 0, $entry";
			}
			else
			{
				$query = "SELECT * FROM inventory WHERE ITEM_NAME LIKE '%$kw%' ORDER BY ITEM_BUY_DATE $sort LIMIT 0, 10";
			}
		}

		$result = $this->db->query($query);
		$data = $result->result_array();
		$n_data = $result->num_rows();

		return array('item_data' => $data, 'n_data' => $n_data);
	}

	public function get_data_by_id($id)
	{
//		$this->db->where('item_id', $id);
//		$data = $this->db->get('inventory');
		$data = $this->db->query("SELECT * FROM inventory WHERE ITEM_ID = '$id' AND ITEM_ACTIVE = 'Y'");

		if ($data->num_rows() >= 1)
			return $data->result_array();
		else
			return FALSE;
	}

	// THIS IS FOR inventory CATEGORY
	public function manage_category()
	{
		// set default message
		$msg = "";
		// get the process
		$pcs = $this->input->post('pcs');
		// temp category name
		$ctg_name = trim(ucwords($this->input->post('ctg_name')));

		switch ($pcs)
		{
			case 'add':
					$data = array(
									'CTG_NAME'	=>	$ctg_name
					);

					$result = $this->db->insert('inventory_ctg', $data);

					if (!$result)
						$msg = "Gagal menambahkan kategori.";
					else
						$msg = "Kategori berhasil ditambah.";
					break;
			case 'edit':
					$ctg_id = $this->input->post('ctg_id');
					$data = array(
									'CTG_NAME'	=>	$ctg_name
					);

					$this->db->where('CTG_ID', $ctg_id);
					$result = $this->db->update('inventory_ctg', $data);

					if (!$result)
						$msg = "Gagal mengedit kategori.";
					else
						$msg = "Kategori berhasil diedit.";
					break;
			case 'delete':
					$ctg_id = $this->input->post('ctg_id');
					$data = array(
									'CTG_NAME'	=>	"-"
					);

					$this->db->where('CTG_ID', $ctg_id);
					$result = $this->db->update('inventory_ctg', $data);

					if (!$result)
						$msg = "Gagal menghapus kategori.";
					else
						$msg = "Kategori berhasil dihapus.";
					break;
			default:;break;
		}
	}

	public function check_ctg()
	{
		$ctg_name = $this->input->post('ctg_name');
		$ctg_name = trim(ucwords($ctg_name));
		$check = $this->db->query("SELECT * FROM inventory_ctg WHERE CTG_NAME = '$ctg_name'");
		if ($check->num_rows() >= 1)
			return FALSE;
		else
			return TRUE;
	}

	public function get_ctg()
	{
		$result = $this->db->query('SELECT * FROM inventory_ctg ORDER BY CTG_NAME ASC');
		return $result->result_array();
	}
}

?>
