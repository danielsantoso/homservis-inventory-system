<?php

class Login_Model extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function validate_user()
	{
		// temp user info into variables
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$user_pwd = $this->security->xss_clean($this->input->post('user_pwd'));
		$user_pwd = sha1($user_pwd);
		$session_id = session_id();
		$ip_address = $_SERVER['REMOTE_ADDR'];
		$user_agent = $this->input->user_agent();


		$query = "SELECT * FROM user_account WHERE USER_ID = '$user_id' AND USER_PASSWORD = '$user_pwd'";
		$get = $this->db->query($query);

		if ($get->num_rows() == 1)
		{
			// create login id
			$login_id = $user_id.mktime();

			// If true, then save login info to db and create session for user
			$data = array(
							'LOGIN_ID'		=>	$login_id,
							'USER_ID'			=>	$user_id,
							'SESSION_ID'	=>	$session_id,
							'IP_ADDRESS'	=>	$ip_address,
							'BROWSER'			=>	$user_agent
			);

			$result = $this->db->insert('user_log', $data);

			// get username from db
			$row = $get->row();
			$user_name = $row->USER_NAME;
			$user_role = $row->USER_ROLE;
			$data = array(
							'login_id'				=> $login_id,
							'user_id'					=> $user_id,
							'user_name'				=> $user_name,
							'dashboard_access'=> TRUE,	
							'user_role'				=> $user_role
			);

			$this->session->set_userdata($data);

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function logout()
	{
		$login_id = $this->session->userdata('login_id');

		// Update Logout activity
		$data = array(
				'LOGOUT_TIME'	=> date('Y-m-d H:i:s')
				);

		$this->db->where('LOGIN_ID', $login_id);
		$this->db->update('user_log', $data);
	}
}

?>
