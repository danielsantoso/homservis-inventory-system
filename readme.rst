##########################
Homservis Inventory System
##########################

Homservis Inventory System adalah Sistem Inventory milik Homservis yang digunakan untuk melakukan manajemen asset milik Homservis. Dengan sistem ini, manajemen asset pada perusahaan Homservis menjadi lebih mudah, teratur, dan aman. 
Berikut adalah fitur-fitur yang terdapat dalam sistem ini :

* Kelola Asset
* Kelola Akun Pengguna
* Kelola Kategori 

***************
Informasi Rilis
***************
Versi 1.1

Ini adalah versi pertama dari pengembangan sistem ini. Untuk melihat demo nya, dapat langsung mengunjungi [Homservis Inventory System]  (http://inv.homservis.com)

**************************
Teknologi yang digunakan
**************************
- Codeigniter versi 3.0.6
- Materialize CSS
- Dropzone JS
- JQuery 2.2.4
- JQuery Data Table

*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

******************
Petunjuk Instalasi
******************

Berikut adalah panduan instalasi Homservis Inventory System :

- Download package Homservis Inventory System 
- Taruh package di folder htdocs (Jika menggunakan XAMPP)
- Buka folder db di dalamnya, lalu import homservis_inv_db ke komputer Anda
- Buka folder application/config/config.php, lalu ubah 
  $config['base_url'] = 'http://localhost/homservis/inventory2' menjadi base url Anda
- Buka folder application/config/database.php, lalu ubah settingan database sesuai konfigurasi komputer Anda
- Sistem siap digunakan

***************
Acknowledgement
***************

Sistem Inventory ini dikembangkan oleh Daniel Santoso.